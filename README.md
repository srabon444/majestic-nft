# Majestic NFT

Majestic NFT is a platform where you can mint your own NFTs and sell them. You can also buy NFTs from other users. The platform is built using Next.js, React, Typescript, Tailwind, and Vercel.

## [Live Application](https://majestic-nft.vercel.app/)

![App Screenshot 1](public/images/1.png)
![App Screenshot 2](public/images/2.png)
![App Screenshot 3](public/images/3.png)
![App Screenshot 4](public/images/4.png)

## Features

- Buy NFTs
- View Wallet
- View NFTs
- View NFT Details

## Tech Stack

- Next.js
- React
- Typescript
- Tailwind
- Vercel

**Hosting:**

- Vercel

## Run Locally

Clone the project

```bash
  https://gitlab.com/srabon444/majestic-nft.git
```

Install dependencies

```bash
  npm install
```
