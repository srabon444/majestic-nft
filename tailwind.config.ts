import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1360px",
    },

    fontSize: {
      xs: "12px",
      sm: "16px",
      base: "20px",
      lg: "24px",
      xl: "32px",
      "2xl": "64px",
    },

    extend: {
      colors: {
        black: {
          dark: "#000000",
          light: "#23252B",
        },
        gray: {
          default: "#617587",
          dark: "#23252B",
        },
        green: "#E1EDD9",
        blue: {
          default: "#29627F",
          dark: "#D4D3EB",
          light: "#E6E9F2",
        },
        white: "#FFFFFF",
        pink: "#FADFE4",
      },
      boxShadow: {
        "green-glow": "0 0 40px #E1EDD9, 0 0 20px #E1EDD9",
      },
      zIndex: {
        "10": "10",
        "20": "20",
        "30": "30",
        "40": "40",
        "50": "50",
      },
    },
  },
  plugins: [],
};
export default config;
