import { TArtistData } from "@/lib/types";
import Hero from "@/components/hero";
import Collection from "@/components/collection";

export default async function Page({ params }: { params: { slug: string } }) {
  const slug = params.slug;
  const artistCollection = await getCollectionData(slug);

  return (
    <>
      <Hero currentArtist={artistCollection} heroType={"collection"} />
      <Collection
        nftsData={artistCollection.nfts}
        title={"NFTs"}
        cardType={"nft"}
      />
    </>
  );
}

const getCollectionData = async (slug: string) => {
  try {
    const response = await fetch(
      "https://majestic-nft-api.onrender.com/artistsCollection",
    );
    const artistsData = await response.json();
    const slugData = artistsData.find(
      (artist: TArtistData) => artist.slug === slug,
    );
    return slugData === undefined ? null : slugData;
  } catch (error) {
    console.error("Failed to fetch artist data:", error);
  }
};
