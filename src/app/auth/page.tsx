"use client";

import Paragraph from "@/components/paragraph";
import { useNft } from "@/contexts/useNft";

export default function Page() {
  const { connectToWalletHandler } = useNft();
  return (
    <section className="w-full max-w-[700px] flex flex-col mx-auto pt-[100px] h-screen">
      <Paragraph
        text={"Choose the wallet to connect"}
        sx={"font-bold text-[32px] text-center"}
      />

      <div className={"grid grid-cols-3 gap-x-[60px] mt-[90px]"}>
        <div
          className="bg-white w-full h-[175px] rounded-[20px] hover:cursor-pointer"
          onClick={connectToWalletHandler}
        />
        <div
          className="bg-white w-full h-[175px] rounded-[20px] hover:cursor-pointer"
          onClick={connectToWalletHandler}
        />
        <div
          className="bg-white w-full h-[175px] rounded-[20px] hover:cursor-pointer"
          onClick={connectToWalletHandler}
        />
      </div>
    </section>
  );
}
