"use client";

import HeroTopStyle from "@/components/hero-top-style";
import Hero from "@/components/hero";
import { useArtist } from "@/contexts/useArtist";
import Collection from "@/components/collection";

export default function Home() {
  const { currentArtist, artistsData } = useArtist();

  return (
    <section>
      <HeroTopStyle />
      {currentArtist && (
        <Hero currentArtist={currentArtist} heroType={"home"} />
      )}

      {artistsData.length > 0 ? (
        <Collection
          cardType={"collection"}
          title={"Collections"}
          artistsData={artistsData}
        />
      ) : null}
    </section>
  );
}
