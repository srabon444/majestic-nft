import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "@/styles/globals.css";
import Header from "@/components/layout/header";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";
import ArtistContextProvider from "@/contexts/useArtist";
import UseNftContextProvider from "@/contexts/useNft";
import DefaultLayout from "@/components/layout/default-layout";

config.autoAddCss = false;

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Majestic NFT",
  description: "Majestic NFT is a #1 marketplace for unique NFTs.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`${inter.className} bg-white`}>
        <ArtistContextProvider>
          <UseNftContextProvider>
            <Header />
            <DefaultLayout>{children}</DefaultLayout>
          </UseNftContextProvider>
        </ArtistContextProvider>
      </body>
    </html>
  );
}
