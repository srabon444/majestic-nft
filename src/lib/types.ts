import { Dispatch, SetStateAction } from "react";

export type ButtonProps = {
  text: string;
  sx?: string;
  children?: React.ReactNode;
  onClick?: () => void;
};

export type TitleProps = {
  title: string;
  sx?: string;
};

export type ParagraphProps = {
  text: string;
  sx?: string;
};

export type InfoChipProps = {
  text: string;
  sx?: string;
};

export type TArtistContextType = {
  artistsData: TArtistData[];
  currentArtist: TArtistData | null;
};

export type TNftContextType = {
  nftsData: TNftData[] | [];
  setNftsData: Dispatch<SetStateAction<TNftData[] | []>>;
  addToCartHandler: (nft: TNftData) => void;
  removeFromCartHandler: (nft: TNftData) => void;
  showCart: boolean;
  setShowCart: Dispatch<SetStateAction<boolean>>;
  toggleCart: () => void;
  isAuthenticate: boolean;
  connectToWalletHandler: () => void;
};

export type TArtistData = {
  id: number;
  artistName: string;
  slug: string;
  artistImage: string;
  collectionName: string;
  collectionDescription: string;
  collectionCoverImage: string;
  quantity: number;
  priceRange: {
    min: string;
    max: string;
  };
  nfts: TNftData[];
};

export type TNftData = {
  nftId: number;
  nftName: string;
  image: string;
  price: string;
};

export type ArtistProfileProps = ArtistInfo & {
  width?: number;
  height?: number;
};

export type HeroProps = {
  currentArtist: TArtistData;
  heroType: HeroType;
};

export type ArtistInfo = {
  name: string;
  image: string;
};

export type CollectionProps = {
  artistsData?: TArtistData[];
  nftsData?: TNftData[];
  title: string;
  cardType: "collection" | "nft";
};

export type ArtDetailsCardProps = {
  artData?: TArtistData | null;
  nftData?: TNftData | null;
  cardType: "nft" | "collection";
};

export type HeroType = "home" | "collection";

export type NftListProps = {
  artist: TArtistData;
};

export type CartProps = {
  showCart: boolean;
  setShowCart: Dispatch<SetStateAction<boolean>>;
};
