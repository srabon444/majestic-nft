"use client";

import React, { createContext, useContext, useState } from "react";
import { TNftContextType, TNftData } from "@/lib/types";
import { useRouter } from "next/navigation";

const initialNftData = {
  nftsData: [],
  setNftsData: () => {},
  addToCartHandler: (nft: TNftData) => {},
  removeFromCartHandler: (nft: TNftData) => {},
  showCart: false,
  setShowCart: () => {},
  toggleCart: () => {},
  isAuthenticate: false,
  connectToWalletHandler: () => {},
};

const NftDataContext = createContext<TNftContextType>(initialNftData);

export const useNft = () => useContext(NftDataContext);

export default function UseNftContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const router = useRouter();
  const [nftsData, setNftsData] = useState<TNftData[] | []>([]);
  const [showCart, setShowCart] = useState(false);
  const [isAuthenticate, setIsAuthenticate] = useState(false);

  const toggleCart = () => {
    if (isAuthenticate) {
      setShowCart(!showCart);
    } else {
      router.push("/auth");
    }
  };

  // Local Storage persist usage
  // useEffect(() => {
  //     if (artistsData.length === 0) {
  //         getArtistData();
  //     }
  // }, [artistsData]);

  const addToCartHandler = (nft: TNftData) => {
    if (!isAuthenticate) {
      router.push("/auth");
    }

    setNftsData([...nftsData, nft]);
  };

  const removeFromCartHandler = (nftToRemove: TNftData) => {
    setNftsData(nftsData.filter((nft) => nft !== nftToRemove));
  };

  const connectToWalletHandler = () => {
    setIsAuthenticate(true);
    router.back();
  };

  return (
    <NftDataContext.Provider
      value={{
        nftsData,
        setNftsData,
        showCart,
        addToCartHandler,
        removeFromCartHandler,
        toggleCart,
        setShowCart,
        isAuthenticate,
        connectToWalletHandler,
      }}
    >
      {children}
    </NftDataContext.Provider>
  );
}
