"use client";

import React, { createContext, useContext, useEffect, useState } from "react";
import { TArtistContextType, TArtistData } from "@/lib/types";

const initialArtistData = {
  artistsData: [],
  currentArtist: null,
};

const ArtistDataContext = createContext<TArtistContextType>(initialArtistData);

export const useArtist = () => useContext(ArtistDataContext);

export default function UseArtistContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [artistsData, setArtistData] = useState<TArtistData[] | []>([]);
  const [currentArtist, setCurrentArtist] = useState<TArtistData | null>(null);

  useEffect(() => {
    if (artistsData.length === 0) {
      getArtistData();
    }
  }, [artistsData]);

  const getArtistData = async () => {
    try {
      let response = await fetch(
        "https://majestic-nft-api.onrender.com/artistsCollection",
      );
      let data = await response.json();
      setArtistData(data);
      setCurrentArtist(data[0]);
    } catch (error) {
      console.error("Failed to fetch artist data:", error);
    }
  };

  return (
    <ArtistDataContext.Provider
      value={{
        artistsData,
        currentArtist,
      }}
    >
      {children}
    </ArtistDataContext.Provider>
  );
}
