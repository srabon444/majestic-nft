import { InfoChipProps } from "@/lib/types";
import { cn } from "@/lib/utils";

export default function InfoChip({ text, sx }: InfoChipProps) {
  return (
    <div
      className={cn(
        "max-w-[150px] h-[33px] rounded-[40px] text-xs text-black-light bg-green flex items-center justify-center",
        sx,
      )}
    >
      {text}
    </div>
  );
}
