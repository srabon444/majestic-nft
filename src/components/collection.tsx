import Title from "@/components/title";
import { CollectionProps } from "@/lib/types";
import ArtDetailsCard from "@/components/cards/art-details-card";

export default function Collection({
  artistsData,
  cardType,
  title,
  nftsData,
}: CollectionProps) {

  return (
    <section className="flex flex-col mt-12 sm:mt-[131px] mb-16 sm:mb-32 rounded-[47px]">
      <Title title={title} />
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-y-8 gap-x-10 mt-5 sm:mt-11">
        {artistsData &&
          cardType === "collection" &&
          artistsData.map((artist, index) => {
            return (
              <ArtDetailsCard
                artData={artist}
                cardType={cardType}
                key={index}
              />
            );
          })}

        {nftsData &&
          cardType === "nft" &&
          nftsData.map((nft, index) => {
            return (
              <ArtDetailsCard nftData={nft} cardType={cardType} key={index} />
            );
          })}
      </div>
    </section>
  );
}
