import { ParagraphProps } from "@/lib/types";
import { cn } from "@/lib/utils";

export default function Paragraph({ text, sx }: ParagraphProps) {
  return (
    <p className={cn("text-sm leading-[150%] text-black-light", sx)}>{text}</p>
  );
}
