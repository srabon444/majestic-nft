import ArtistProfile from "@/components/artist-profile";
import { HeroProps } from "@/lib/types";
import InfoChip from "@/components/info-chip";
import Paragraph from "@/components/paragraph";
import Button from "@/components/button";
import Image from "next/image";
import { cn } from "@/lib/utils";

export default function Hero({ currentArtist, heroType }: HeroProps) {
  return (
    <section
      className={cn(
        "grid grid-cols-1 lg:grid-cols-[2fr_1fr] gap-y-12  lg:gap-x-28 w-full place-content-between items-center bg-green rounded-[30px] px-5 lg:px-[75px] sm:py-[45px] py-2 mt-[15px]",
        {
          "bg-white": heroType === "collection",
        },
      )}
    >
      <div className="">
        <InfoChip text={"Trending Now"} sx="bg-pink mb-9" />

        <Paragraph
          text={
            heroType === "home"
              ? `${currentArtist.collectionName} collection`
              : "Collection"
          }
          sx="text-gray-default text-md sm:text-lg leading-none"
        />

        <h1 className="text-black-dark text-xl sm:text-2xl font-extrabold leading-tight sm:mb-0 mb-2">
          {heroType === "home"
            ? currentArtist.nfts[1].nftName
            : currentArtist.collectionName}
        </h1>

        {heroType === "collection" && (
          <Paragraph
            text={currentArtist.collectionDescription}
            sx="text-black-light leading-[227%]  sm:mt-4 mb-7 text-sm"
          />
        )}

        <ArtistProfile
          name={currentArtist.artistName}
          image={currentArtist.artistImage}
        />

        {heroType === "home" && (
          <div className="flex items-center mt-6 sm:mt-11 gap-x-5">
            <Button
              sx="text-white text-xs sm:text-sm bg-black-dark font-semibold"
              text={"Buy"}
            />
            <Button
              sx="text-black-light text-xs sm:text-sm font-semibold border bg-transparent border-black-light"
              text={"See collection"}
            />
          </div>
        )}
      </div>

      <Image
        src={currentArtist.nfts[1].image}
        alt={currentArtist.nfts[1].nftName}
        width={437}
        height={411}
        className="rounded-[52px] mx-auto"
      />
    </section>
  );
}
