import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleArrowRight } from "@fortawesome/free-solid-svg-icons";
import { faClone, faTrashCan } from "@fortawesome/free-regular-svg-icons";
import Paragraph from "@/components/paragraph";
import Title from "@/components/title";
import Button from "@/components/button";
import { cn } from "@/lib/utils";
import { useNft } from "@/contexts/useNft";
import Image from "next/image";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Cart() {
  const {
    showCart,
    setShowCart,
    nftsData,
    setNftsData,
    removeFromCartHandler,
  } = useNft();

  const crypto = require("crypto");

  const generateUserId = () => {
    const id = crypto.randomBytes(15).toString("base64").slice(0, 15);
    return id.length > 10 ? `${id.slice(0, 10)}...` : id;
  };

  const totalPrice = nftsData.reduce(
    (acc, nft) => acc + parseFloat(nft.price),
    0,
  );

  return (
    <>
      <ToastContainer />
      <section
        className={cn(
          "border-2 border-black-dark w-[300px] sm:w-[492px] h-[981px] absolute top-0 right-0 z-50 bg-white rounded-3xl p-7",
          {
            hidden: !showCart,
            block: showCart,
          },
        )}
      >
        <div className="flex sm:flex-row flex-col sm:justify-between gap-y-3 items-center">
          <div className="flex items-center gap-x-2">
            <div className="rounded-full w-[50px] h-[50px] bg-blue-default" />
            {/*  Need to change the uid*/}
            <Paragraph text={"zyFLfY76mg..."} />
            <FontAwesomeIcon className="w-5 h-5" icon={faClone} />
          </div>
          <FontAwesomeIcon
            className="mr-2 w-[30px] h-[30px]"
            icon={faCircleArrowRight}
          />
        </div>

        <div className="sm:mt-16 mt-6 mb-6 sm:mb-24">
          <Paragraph
            sx="text-[14px] leading-[150%] text-[#7B7B7B] tracking-[5%]"
            text={"In your wallet"}
          />
          <Title
            title={`${totalPrice.toFixed(2)} BTC`}
            sx="sm:text-[36px] text-xl text-[#02071D] font-semibold sm:leading-[168%]"
          />
        </div>
        <Title
          sx={cn("sm:mt-24 sm:mb-[104px]", {
            "sm:mb-7 mb-4": nftsData.length > 0,
          })}
          title={"Your NFTs"}
        />

        {nftsData.length === 0 ? (
          <div className="flex items-center flex-col">
            <Paragraph
              sx="text-gray-default text-lg leading-none mb-16"
              text={"You don’t own any NFTs yet"}
            />
            <Button
              onClick={() => setShowCart(false)}
              sx="bg-black-dark text-white"
              text={"Start Shopping"}
            />
          </div>
        ) : (
          <div className="flex flex-col gap-y-5 overflow-y-auto h-[670px] sm:h-[550px]">
            {nftsData.map((nft, index) => (
              <div className="relative" key={index}>
                <Image
                  src={nft.image}
                  alt={nft.nftName}
                  width={100}
                  height={100}
                  className="w-full h-[227px] rounded-3xl"
                />
                <FontAwesomeIcon
                  icon={faTrashCan}
                  size="lg"
                  className="absolute top-4 right-6 text-red-600 hover:scale-110 cursor-pointer"
                  onClick={() => removeFromCartHandler(nft)}
                />
              </div>
            ))}
            <Button
              onClick={() => {
                toast("You've checked out successfully!", {
                  className: "toast-success",
                  progressClassName: "toast-progress",
                });
                setNftsData([]);
                setShowCart(false);
              }}
              sx="mt-4 bg-blue-800 text-white flex-shrink-0 min-w-full mx-auto"
              text={"Checkout"}
            />
          </div>
        )}
      </section>
    </>
  );
}
