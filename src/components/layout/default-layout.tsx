"use client";

import Footer from "@/components/layout/footer";
import { useNft } from "@/contexts/useNft";
import { cn } from "@/lib/utils";
import { usePathname } from "next/navigation";

export default function DefaultLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const { isAuthenticate } = useNft();
  const currentPath = usePathname();

  return (
    <>
      <main
        className={cn(
          "min-h-screen mx-auto px-5 bg-white lg:px-[80px] xl:px-[20px] xl:max-w-[1360px]",
          {
            "bg-white": isAuthenticate && currentPath !== "/auth",
            "bg-green !max-w-full": !isAuthenticate && currentPath === "/auth",
          },
        )}
      >
        {children}
      </main>
      {currentPath !== "/auth" && <Footer />}
    </>
  );
}
