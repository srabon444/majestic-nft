"use client";

import Title from "@/components/title";
import Button from "@/components/button";
import Link from "next/link";
import Cart from "@/components/cart";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAnglesRight } from "@fortawesome/free-solid-svg-icons";
import { useNft } from "@/contexts/useNft";
import { cn } from "@/lib/utils";
import { usePathname } from "next/navigation";

export default function Header() {
  const currentPath = usePathname();
  const { toggleCart, showCart, isAuthenticate } = useNft();

  return (
    <header
      className={cn({
        "px-5 lg:px-[80px] xl:px-[20px] relative mx-auto max-w-[1360px] flex justify-between items-center pt-8 pb-8 sm:pb-14 gap-x-5":
          isAuthenticate,
        "max-w-full bg-green pt-8 pb-8 px-4 lg:px-[80px] flex justify-between items-center":
          !isAuthenticate,
        "bg-white gap-x-5 justify-between": currentPath !== "/auth",
      })}
    >
      <Link href={"/"}>
        <Title sx="uppercase" title={"Marketplace."} />
      </Link>

      <div className="relative">
        <Button
          onClick={toggleCart}
          text={isAuthenticate ? "Account" : "Connect Wallet"}
          sx={
            "relative z-10 text-black-light text-xs sm:text-sm font-semibold border bg-transparent border-black-light"
          }
        />

        {showCart && (
          <FontAwesomeIcon
            onClick={() => toggleCart()}
            className="absolute top-[40px] right-[315px] sm:right-[500px] text-sm cursor-pointer"
            icon={faAnglesRight}
          />
        )}

        <Cart />
      </div>
    </header>
  );
}
