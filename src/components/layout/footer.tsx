import Title from "@/components/title";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDiscord,
  faFacebookF,
  faInstagram,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

export default function Footer() {
  return (
    <footer className="mx-auto mt-auto w-full h-[96px] px-12 sm:py-0 py-3 flex sm:flex-row flex-col justify-between items-center bg-green">
      <Title sx="uppercase" title={"Marketplace."} />
      <div className="flex gap-x-[42px]">
        <FontAwesomeIcon width={22} height={24} icon={faFacebookF} />
        <FontAwesomeIcon width={22} height={24} icon={faTwitter} />
        <FontAwesomeIcon width={22} height={24} icon={faDiscord} />
        <FontAwesomeIcon width={22} height={24} icon={faInstagram} />
      </div>
    </footer>
  );
}
