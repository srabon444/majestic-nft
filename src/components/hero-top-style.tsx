export default function HeroTopStyle() {
  return (
    <div className="mx-auto flex gap-x-8 max-w-[1304px]">
      <div className="w-48 h-[14px] bg-black-light rounded-full" />
      <div className="relative">
        <div className="w-48 h-[14px] bg-green rounded-full" />
        <div className="absolute top-0 left-0 w-[46px] h-[14px] bg-black-light rounded-full" />
      </div>
      <div className="w-48 h-[14px] bg-green rounded-full" />
      <div className="w-48 h-[14px] bg-green rounded-full" />
      <div className="w-48 h-[14px] bg-green rounded-full" />
      <div className="w-48 h-[14px] bg-green rounded-full" />
    </div>
  );
}
