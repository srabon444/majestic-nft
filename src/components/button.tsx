"use client";

import { ButtonProps } from "@/lib/types";
import { cn } from "@/lib/utils";

export default function Button({ text, sx, children, onClick }: ButtonProps) {
  return (
    <button
      onClick={() => onClick && onClick()}
      className={cn(
        "w-full sm:p-0 p-3 sm:w-[203px] h-auto sm:h-[65px] rounded-full text-black-light text-sm font-semibold text-center",
        sx,
      )}
    >
      {text}
      {children}
    </button>
  );
}
