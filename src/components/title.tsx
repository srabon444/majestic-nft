import { TitleProps } from "@/lib/types";
import { cn } from "@/lib/utils";

export default function Title({ title, sx }: TitleProps) {
  return (
    <h3 className={cn("sm:text-lg text-base text-black-dark font-bold", sx)}>{title}</h3>
  );
}
