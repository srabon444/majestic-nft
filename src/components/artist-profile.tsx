"use client";

import Image from "next/image";
import { ArtistProfileProps } from "@/lib/types";

export default function ArtistProfile({ name, image }: ArtistProfileProps) {
  if (!name || !image) return null;

  return (
    <div className="flex items-center gap-x-4">
      <Image
        width={100}
        height={100}
        src={image}
        className={`sm:w-[68px] sm:h-[68px] w-12 h-12`}
        alt="Artist Image"
      />
      <div className="flex-wrap">
        <p className="text-xs text-gray-default leading-tight">Artist</p>
        <p className="sm:text-lg text-sm text-black-light leading-tight">{name}</p>
      </div>
    </div>
  );
}
