"use client";

import Image from "next/image";
import Title from "@/components/title";
import InfoChip from "@/components/info-chip";
import Paragraph from "@/components/paragraph";
import ArtistProfile from "@/components/artist-profile";
import Button from "@/components/button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightLong } from "@fortawesome/free-solid-svg-icons";
import { ArtDetailsCardProps } from "@/lib/types";
import { useRouter } from "next/navigation";
import { useNft } from "@/contexts/useNft";

export default function ArtDetailsCard({
  artData,
  nftData,
  cardType,
}: ArtDetailsCardProps) {
  const { addToCartHandler, setShowCart, isAuthenticate } = useNft();
  const router = useRouter();

  const onClickHandler = () => {
    if (cardType === "collection" && artData?.slug) {
      router.push(`/collection/${artData?.slug}`);
    } else if (cardType === "nft" && isAuthenticate) {
      nftData && addToCartHandler(nftData);
      setShowCart(true);
      // window.scrollTo(0, 0);
    } else if (cardType === "nft" && !isAuthenticate) {
      router.push("/auth");
    }
  };

  return (
    <>
      <div
        id="art-details"
        className="flex flex-col mx-auto shadow-green-glow rounded-3xl bg-white p-4 pb-8 gap-y-7"
      >
        {cardType === "collection" && artData && (
          <>
            <div className="relative group flex justify-center items-center">
              <Image
                src={artData.collectionCoverImage}
                alt={artData.collectionName}
                width={100}
                height={100}
                className={
                  "transition-opacity duration-200 group-hover:opacity-50 rounded-3xl w-[391px] h-full sm:h-[227px]"
                }
              />

              <Button
                onClick={onClickHandler}
                sx="absolute opacity-0 group-hover:opacity-100 transition-opacity duration-200 box-border flex items-center justify-center gap-x-2 w-[248px] h-[67px] bg-white border-none"
                text={cardType === "collection" ? "Go to collection" : "Buy"}
              >
                <FontAwesomeIcon icon={faArrowRightLong} />
              </Button>
            </div>
            <div className="flex flex-row justify-between items-center max-w-[391px]">
              <Title
                title={artData.collectionName}
                sx="text-lg text-black-dark font-bold"
              />
              <InfoChip
                text={`${artData.quantity} NFT`}
                sx={"bg-green w-[118px] text-gray-dark rounded-[49px]"}
              />
            </div>
            <div className="mb-2">
              <Paragraph
                text={`Price Range : ${artData.priceRange.min}BTC - ${artData.priceRange.max}BTC`}
                sx="text-gray-default text-[14px] text-gray-default"
              />
              <Paragraph
                text={`${artData.collectionDescription.substring(0, 80)}...`}
                sx="text-gray-default text-sm  leading-[150%] text-black-light max-w-[391px]"
              />
            </div>
            <ArtistProfile
              name={artData.artistName}
              image={artData.artistImage}
              width={40}
              height={40}
            />
          </>
        )}

        {cardType === "nft" && nftData && (
          <>
            <div className="relative group flex justify-center items-center">
              <Image
                src={nftData?.image}
                alt={nftData?.nftName}
                width={100}
                height={100}
                className={
                  "transition-opacity duration-200 group-hover:opacity-50 rounded-3xl w-[391px] h-full sm:h-[345px]"
                }
              />

              <Button
                onClick={onClickHandler}
                sx="absolute opacity-0 group-hover:opacity-100 transition-opacity duration-200 box-border flex items-center justify-center gap-x-2 w-[248px] h-[67px] bg-white border-none"
                text={"Buy"}
              >
                <FontAwesomeIcon icon={faArrowRightLong} />
              </Button>
            </div>

            <div className="flex flex-row justify-between items-center max-w-[391px]">
              <Title
                title={nftData.nftName}
                sx="text-lg text-black-dark font-bold"
              />
              <InfoChip
                text={`${nftData.price} BTC`}
                sx={"bg-blue-dark w-[118px] text-gray-dark rounded-[49px]"}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
}
